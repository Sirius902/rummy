package com.gitlab.sirius902.rummy

import cats.effect.{ExitCode, IO, IOApp, Sync}
import cats.implicits._
import com.gitlab.sirius902.rummy.game.Rummy
import com.gitlab.sirius902.rummy.io.MyIO._

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = program[IO].as(ExitCode.Success)

  def program[F[_] : Sync]: F[Unit] =
    for {
      _ <- writeLine[F]("Welcome to Rummy!")
      _ <- readLine[F]("Press Enter to begin...")
      _ <- Rummy.start[F]
    } yield ()
}
