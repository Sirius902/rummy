package com.gitlab.sirius902.rummy.debugpkg

import cats.data._
import cats.effect.Sync
import com.gitlab.sirius902.rummy.card.Card
import com.gitlab.sirius902.rummy.game.GameState
import com.gitlab.sirius902.rummy.game.Rummy.Game
import com.gitlab.sirius902.rummy.io.RummyIO._

object RummyDebug {
  def printDeckChanges[F[_] : Sync]: Game[F, Unit] =
    for {
      gs <- StateT.get[F, GameState]
      str <- StateT.pure[F, GameState, String](
        s"Cards Left: ${gs.deck.length}, Cards Removed: ${Card.all.length - gs.deck.length}"
      )
      _ <- writeLine[F](str)
    } yield ()
}
