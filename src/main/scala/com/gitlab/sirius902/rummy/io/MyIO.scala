package com.gitlab.sirius902.rummy.io

import cats.effect.Sync

import scala.collection.generic.CanBuildFrom
import scala.io.StdIn
import scala.util.Random

object MyIO {
  def write[F[_] : Sync](x: Any): F[Unit] =
    Sync[F].delay(print(x))

  def writeLine[F[_] : Sync](x: Any): F[Unit] =
    Sync[F].delay(println(x))

  def writeLine[F[_] : Sync](): F[Unit] =
    Sync[F].delay(println)

  def readLine[F[_] : Sync](text: String): F[String] =
    Sync[F].delay(StdIn.readLine(text))

  def readLine[F[_] : Sync]: F[String] =
    Sync[F].delay(StdIn.readLine)

  def shuffle[F[_] : Sync, A[X] <: TraversableOnce[X], B](bs: A[B])(implicit bf: CanBuildFrom[A[B], B, A[B]]): F[A[B]] =
    Sync[F].delay(Random.shuffle(bs))
}
