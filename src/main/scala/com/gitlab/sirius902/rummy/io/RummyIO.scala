package com.gitlab.sirius902.rummy.io

import cats.data.StateT
import cats.effect.Sync
import com.gitlab.sirius902.rummy.game.GameState
import com.gitlab.sirius902.rummy.game.Rummy.Game

import scala.collection.generic.CanBuildFrom

object RummyIO {
  def write[F[_] : Sync](x: Any): Game[F, Unit] =
    StateT.liftF[F, GameState, Unit](MyIO.write[F](x))

  def writeLine[F[_] : Sync](x: Any): Game[F, Unit] =
    StateT.liftF[F, GameState, Unit](MyIO.writeLine[F](x))

  def writeLine[F[_] : Sync](): Game[F, Unit] =
    StateT.liftF[F, GameState, Unit](MyIO.writeLine[F]())

  def readLine[F[_] : Sync]: Game[F, String] =
    StateT.liftF[F, GameState, String](MyIO.readLine[F])

  def readLine[F[_] : Sync](text: String): Game[F, String] =
    StateT.liftF[F, GameState, String](MyIO.readLine[F](text))

  def shuffle[F[_] : Sync, A[X] <: TraversableOnce[X], B](bs: A[B])
                                                         (implicit bf: CanBuildFrom[A[B], B, A[B]]): Game[F, A[B]] =
    StateT.liftF[F, GameState, A[B]](MyIO.shuffle[F, A, B](bs))
}
