package com.gitlab.sirius902.rummy.card

import cats.implicits._

sealed trait Rank {
  val orderings: Set[Int]
  val points: Int
  val displaySymbol: String
  val inputSymbol: String
}

object Rank {

  val all: List[Rank] = List(Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace)

  val sequences: List[List[Rank]] = sortCombinations(all)

  // TODO: Use orderings for all cards instead of doing different logic for aces
  // Currently assumes list is distinct
  def sortCombinations(ranks: List[Rank]): List[List[Rank]] = {
    val ace = ranks.find(_ == Ace).toList
    val withoutAce = ranks.distinct.filterNot(_ == Ace)
    val insideSorted = withoutAce.sortBy(_.orderings.head)

    List(
      ace ::: insideSorted,
      insideSorted ::: ace
    ).distinct
  }

  def sort(ranks: List[Rank]): List[Rank] =
    sortCombinations(ranks).find(sorted => sequences.exists(_.containsSlice(sorted))).getOrElse(Nil)

  def isSequential(ranks: List[Rank]): Boolean =
    Rank
      .sortCombinations(ranks)
      .flatMap(sorted => sequences.map(_.containsSlice(sorted)))
      .contains(true)

  trait Numeric extends Rank {
    override val points: Int = 5
  }

  trait Face extends Rank {
    override val points: Int = 10
  }

  case object Two extends Numeric {
    override val orderings: Set[Int] = Set(1)

    override val displaySymbol: String = "2"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Three extends Numeric {
    override val orderings: Set[Int] = Set(2)

    override val displaySymbol: String = "3"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Four extends Numeric {
    override val orderings: Set[Int] = Set(3)

    override val displaySymbol: String = "4"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Five extends Numeric {
    override val orderings: Set[Int] = Set(4)

    override val displaySymbol: String = "5"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Six extends Numeric {
    override val orderings: Set[Int] = Set(5)

    override val displaySymbol: String = "6"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Seven extends Numeric {
    override val orderings: Set[Int] = Set(6)

    override val displaySymbol: String = "7"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Eight extends Numeric {
    override val orderings: Set[Int] = Set(7)

    override val displaySymbol: String = "8"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Nine extends Numeric {
    override val orderings: Set[Int] = Set(8)

    override val displaySymbol: String = "9"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Ten extends Numeric {
    override val orderings: Set[Int] = Set(9)

    override val displaySymbol: String = "10"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Jack extends Face {
    override val orderings: Set[Int] = Set(10)

    override val displaySymbol: String = "J"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Queen extends Face {
    override val orderings: Set[Int] = Set(11)

    override val displaySymbol: String = "Q"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object King extends Face {
    override val orderings: Set[Int] = Set(12)

    override val displaySymbol: String = "K"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

  case object Ace extends Rank {
    override val orderings: Set[Int] = Set(0, 13)

    override val points: Int = 15

    override val displaySymbol: String = "A"

    override val inputSymbol: String = displaySymbol

    override def toString: String = displaySymbol
  }

}
