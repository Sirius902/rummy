package com.gitlab.sirius902.rummy.card

import com.gitlab.sirius902.rummy.algorithm.Algorithms.sameAll

case class Card(rank: Rank, suit: Suit) {
  override def toString: String = rank.toString ++ suit.toString
}

object Card {
  val all: List[Card] = for {
    s <- Suit.all
    r <- Rank.all
  } yield Card(r, s)

  def formatCards(hand: List[Card]): String = s"[ ${hand.mkString(", ")} ]"

  def sameSuit(cards: List[Card]): Boolean = sameAll(cards.map(_.suit))(_ == _)

  def sameRank(cards: List[Card]): Boolean = sameAll(cards.map(_.rank))(_ == _)

  def sortCombinations(cards: List[Card]): List[List[Card]] =
    cards.groupBy(_.suit).flatMap { case (s, cs) =>
      Rank.sortCombinations(cs.map(_.rank)).map {
        _.map { r =>
          Card(r, s)
        }
      }
    }.toList

  def sort(cards: List[Card]): List[Card] =
    sortCombinations(cards).find(cs => Rank.sequences.exists(_.containsSlice(cs.map(_.rank)))).getOrElse(Nil)
}
