package com.gitlab.sirius902.rummy.card

sealed trait Suit {
  val displaySymbol: String
  val inputSymbol: String
}

object Suit {

  val all: List[Suit] = List(Spades, Hearts, Diamonds, Clubs)

  case object Spades extends Suit {
    override val displaySymbol: String = "♠"

    override val inputSymbol: String = "S"

    override def toString: String = displaySymbol
  }

  case object Hearts extends Suit {
    override val displaySymbol: String = "♥"

    override val inputSymbol: String = "H"

    override def toString: String = displaySymbol
  }

  case object Diamonds extends Suit {
    override val displaySymbol: String = "♦"

    override val inputSymbol: String = "D"

    override def toString: String = displaySymbol
  }

  case object Clubs extends Suit {
    override val displaySymbol: String = "♣"

    override val inputSymbol: String = "C"

    override def toString: String = displaySymbol
  }

}
