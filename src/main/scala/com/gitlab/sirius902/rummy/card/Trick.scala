package com.gitlab.sirius902.rummy.card

import com.gitlab.sirius902.rummy.game.Player

case class Trick(cards: List[Card], basedOn: List[Card])

object Trick {
  def isThreeOfAKind(cards: List[Card]): Boolean =
    cards.length >= 3 && Card.sameRank(cards)

  def isRun(cards: List[Card]): Boolean = {
    val sorted = Rank.sortCombinations(cards.map(_.rank))
    cards.length >= 3 && Card.sameSuit(cards) && sorted.exists(s => Rank.sequences.exists(_.containsSlice(s)))
  }

  def isTrick(cards: List[Card]): Boolean =
    isThreeOfAKind(cards) || isRun(cards)

  def formatPoints(points: List[Trick]): String =
    points.map(_.cards).map(Card.formatCards).mkString(", ") match {
      case "" => "None"
      case a @ _ => a
    }

  def canPlayOffTrick(cards: List[Card], trick: Trick): Boolean =
    Trick.isTrick(cards ::: trick.cards ::: trick.basedOn)

  def canPlayOffPoints(cards: List[Card], points: List[Trick]): Boolean =
    points.exists(canPlayOffTrick(cards, _))

  def canPlayOffPlayers(cards: List[Card], players: List[Player]): Boolean =
    canPlayOffPoints(cards, players.flatMap(_.points))

  def fromParent(newTrick: List[Card], parent: Trick): Trick =
    Trick(newTrick, parent.cards ::: parent.basedOn)

  def score(trick: Trick): Int =
    trick.cards.map(_.rank.points).sum
}
