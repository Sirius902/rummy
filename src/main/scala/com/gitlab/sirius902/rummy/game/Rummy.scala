package com.gitlab.sirius902.rummy.game

import cats._
import cats.data._
import cats.effect._
import cats.implicits._
import com.gitlab.sirius902.rummy.card.{Card, Trick}
import com.gitlab.sirius902.rummy.cli.parse.{ActionParser, CardParser}
import com.gitlab.sirius902.rummy.game.GameState.{gameOverLens, playerDisplayIdFromIndex, turnFlagsLens}
import com.gitlab.sirius902.rummy.game.Types.{Deck, Hand}
import com.gitlab.sirius902.rummy.game.actionhandlers.ActionHandlers
import com.gitlab.sirius902.rummy.io.RummyIO._
import com.gitlab.sirius902.rummy.playeraction.Action

object Rummy {
  type Game[F[_], A] = StateT[F, GameState, A]

  def recurse[F[_] : Monad, A](f: Game[F, A]): Game[F, A] =
    f.flatMap(StateT.pure[F, GameState, A])

  def start[F[_] : Sync]: F[Unit] = {
    val entryPoint = for {
      _ <- setup[F]
      _ <- turnLoop[F]
    } yield ()

    entryPoint.runA(GameState.create(players = 2))
  }

  def resetFlags[F[_] : Applicative]: Game[F, Unit] =
    StateT.modify[F, GameState](turnFlagsLens.set(TurnFlags.initial))

  def setupDeck[F[_] : Sync]: Game[F, Unit] =
    for {
      shuffled <- shuffle[F, List, Card](Card.all)
      _ <- StateT.modify[F, GameState](_.copy(deck = shuffled))
    } yield ()

  def setupDiscardPile[F[_] : Monad]: Game[F, Unit] =
    for {
      discard <- draw[F]()
      _ <- StateT.modify[F, GameState](_.copy(discardPile = discard))
    } yield ()

  def setup[F[_] : Sync]: Game[F, Unit] = for {
    _ <- setupDeck[F]
    _ <- dealCards[F](7)
    _ <- setupDiscardPile[F]
  } yield ()

  def turnLoop[F[_] : Sync]: Game[F, Unit] = for {
    _ <- turn[F]
    flags <- StateT.inspect[F, GameState, TurnFlags](_.turnFlags)
    _ <- {
      if (flags.gameOver)
        showGameOver[F]
      else if (flags.quitInvoked)
        writeLine[F]("Game quit.")
      else
        recurse[F, Unit](turnLoop[F])
    }
  } yield ()

  def promptAction[F[_] : Sync]: Game[F, Action] =
    ActionHandlers.readUntilParse[F, Action](
      "What would you like to do? ",
      "Invalid action! Enter help to view actions."
    )(ActionParser.parse)

  // TODO: Ensure you can't have an empty discard pile when this function is called
  def reuseDiscardPile[F[_] : Sync]: Game[F, Unit] =
    for {
      discard <- StateT.inspect[F, GameState, Deck](_.discardPile)
      discardTail <- StateT.pure[F, GameState, List[Card]](discard.tail)
      _ <- {
        if (discardTail.isEmpty)
          StateT.modify[F, GameState](gameOverLens.set(true))
        else {
          for {
            _ <- writeLine[F]("Reusing discard pile, deck is empty...")
            _ <- StateT.modify[F, GameState] { gs =>
              val newDeck = gs.deck ::: discardTail
              gs.copy(deck = newDeck, discardPile = discard.headOption.toList)
            }
          } yield ()
        }
      }
    } yield ()

  def turn[F[_] : Sync]: Game[F, Unit] = {
    def continuePoint: Game[F, Unit] =
      for {
        action <- promptAction[F]
        _ <- handleAction[F](action)
        flags <- StateT.inspect[F, GameState, TurnFlags](_.turnFlags)
        hand <- currentPlayer[F].map(_.hand)
        deck <- StateT.inspect[F, GameState, Deck](_.deck)
        discardPile <- StateT.inspect[F, GameState, List[Card]](_.discardPile)
        _ <- {
          if (flags.quitInvoked)
            StateT.pure[F, GameState, Unit]()
          else if (flags.discarded)
            if (hand.isEmpty || deck.isEmpty && discardPile.length <= 1)
              StateT.modify[F, GameState](gameOverLens.set(true)).flatMap(_ => nextPlayer[F])
            else if (deck.isEmpty)
              reuseDiscardPile[F].flatMap(_ => nextPlayer[F])
            else
              nextPlayer[F]
          else
            recurse[F, Unit](continuePoint)
        }
      } yield ()

    // TODO: Implement clearing screen at beginning of each turn
    for {
      gs <- StateT.get[F, GameState]
      _ <- resetFlags[F]
      displayId <- StateT.pure[F, GameState, String](playerDisplayIdFromIndex(gs.currentPlayerIndex))
      _ <- writeLine[F]("\n" * 1000)
      _ <- writeLine[F](s"Player $displayId, it's your turn.")
      _ <- readLine[F]("Press Enter to continue...")
      _ <- showAllTurnInformation[F]
      _ <- continuePoint
    } yield ()
  }

  def draw[F[_] : Applicative](n: Int = 1): Game[F, List[Card]] =
    StateT(gs => Applicative[F].pure((gs.copy(deck = gs.deck.drop(n)), gs.deck.take(n).reverse)))

  def drawPlayer[F[_] : Monad](player: Player, n: Int = 1): Game[F, Player] =
    for {
      cards <- draw[F](n)
      newHand <- StateT.pure[F, GameState, Hand](player.hand ::: cards)
    } yield player.copy(hand = newHand)

  def dealCards[F[_] : Monad](n: Int): Game[F, Unit] =
    for {
      players <- StateT.inspect[F, GameState, List[Player]](_.players)
      drawnPlayers <- players.traverse(drawPlayer[F](_, n))
      _ <- StateT.modify[F, GameState](_.copy(players = drawnPlayers))
    } yield ()

  def currentPlayer[F[_] : Applicative]: Game[F, Player] =
    StateT.inspect[F, GameState, Player](GameState.getCurrentPlayer)

  def updateCurrentPlayer[F[_] : Applicative](updatedPlayer: Player): Game[F, Unit] =
    StateT.modify[F, GameState](GameState.updateCurrentPlayer(updatedPlayer))

  def modifyCurrentPlayer[F[_] : Monad](f: Player => Player): Game[F, Unit] =
    currentPlayer[F].map(f).flatMap(updateCurrentPlayer[F])

  def nextPlayer[F[_] : Applicative]: Game[F, Unit] =
    StateT.modify[F, GameState](GameState.switchToNextPlayer)

  def showDiscardPile[F[_] : Sync]: Game[F, Unit] =
    for {
      discardPile <- StateT.inspect[F, GameState, List[Card]](_.discardPile)
      _ <- writeLine[F](s"Here is the discard pile: ${Card.formatCards(discardPile)}")
    } yield ()

  def showCurrentHand[F[_] : Sync]: Game[F, Unit] =
    for {
      hand <- currentPlayer[F].map(_.hand)
      _ <- writeLine[F](s"Here is your hand: ${Card.formatCards(hand)}")
    } yield ()

  // TODO: Replace use with showAllPoints
  def showCurrentPlayerPoints[F[_] : Sync]: Game[F, Unit] =
    for {
      p <- currentPlayer[F]
      _ <- writeLine[F](s"Here are your points: ${Trick.formatPoints(p.points)}")
    } yield ()

  def showAllPoints[F[_] : Sync]: Game[F, Unit] =
    for {
      players <- StateT.inspect[F, GameState, List[Player]](_.players)
      points <- StateT.pure[F, GameState, List[(List[Trick], Int)]](
        players.map(_.points).zipWithIndex
      )
      _ <- {
        points.traverse { case (p, i) =>
          writeLine[F](s"Player ${playerDisplayIdFromIndex(i)}'s points: ${Trick.formatPoints(p)}")
        }
      }
    } yield ()

  def showAllTurnInformation[F[_] : Sync]: Game[F, Unit] =
    for {
      _ <- showCurrentHand[F]
      _ <- showDiscardPile[F]
      _ <- showAllPoints[F]
    } yield ()

  def showGameOver[F[_] : Sync]: Game[F, Unit] =
    for {
      players <- StateT.inspect[F, GameState, List[Player]](_.players)
      stats <- StateT.pure[F, GameState, List[(String, Int)]](
        players.indices.map(i => "Player " ++ GameState.playerDisplayIdFromIndex(i))
          .zip(players.map(Player.score)).toList
      )
      _ <- writeLine[F]("Game over!\n--Scores--")
      _ <- stats.traverse { case (x, s) =>
        writeLine[F](s"$x score: $s")
      }
    } yield ()

  def handleAction[F[_] : Sync](action: Action): Game[F, Unit] =
    ActionHandlers.actionToHandler(action)
}
