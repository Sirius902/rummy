package com.gitlab.sirius902.rummy.game

import com.gitlab.sirius902.rummy.card.{Card, Trick}
import com.gitlab.sirius902.rummy.game.Types.Hand

case class Player(
                 hand: Hand,
                 points: List[Trick]
                 )

object Player {
  lazy val initial: Player =
    Player(
      hand = Nil,
      points = Nil
    )

  def score(player: Player): Int =
    player.points.map(Trick.score).sum - player.hand.map(_.rank.points).sum
}
