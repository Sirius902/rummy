package com.gitlab.sirius902.rummy.game.actionhandlers

import cats.data.StateT
import cats.effect._
import com.gitlab.sirius902.rummy.card.{Card, Trick}
import com.gitlab.sirius902.rummy.game.GameState.{discardedLens, hasTakenLens, quitInvokedLens, gameOverLens}
import com.gitlab.sirius902.rummy.game.Rummy._
import com.gitlab.sirius902.rummy.game.Types.Hand
import com.gitlab.sirius902.rummy.game.actionhandlers.Conditions.performOnCondition
import com.gitlab.sirius902.rummy.game.{GameState, Player, TurnFlags}
import com.gitlab.sirius902.rummy.io.RummyIO._
import com.gitlab.sirius902.rummy.playeraction.Action
import com.gitlab.sirius902.rummy.playeraction.Action.View

object ActionHandlers {
  // TODO: Remove
  def notImplemented[F[_] : Sync]: Game[F, Unit] =
    writeLine[F]("Not implemented!")

  def actionToHandler[F[_] : Sync](action: Action): Game[F, Unit] = {
    import com.gitlab.sirius902.rummy.playeraction.Action._

    action match {
      case Draw => draw[F]
      case Discard(card) => discard[F](card)
      case TakeDiscard(untilWithPlay) => takeDiscard[F](untilWithPlay)
      case Points(cards) => points[F](cards)
      case Snatch => snatch[F]
      case Help => help[F]
      case View(what) => view[F](what)
      case QuitGame => quit[F]
    }
  }

  def draw[F[_] : Sync]: Game[F, Unit] =
    performOnCondition[F](Conditions.draw[F]) {
      for {
        player <- currentPlayer[F]
        newPlayer <- drawPlayer[F](player)
        _ <- updateCurrentPlayer[F](newPlayer)
        _ <- StateT.modify[F, GameState](hasTakenLens.set(true))
        _ <- showCurrentHand[F]
      } yield ()
    }

  def discard[F[_] : Sync](card: Card): Game[F, Unit] =
    performOnCondition[F](Conditions.discard[F](card)) {
      for {
        player <- currentPlayer[F]
        gs <- StateT.get[F, GameState]
        newPlayer <- StateT.pure[F, GameState, Player](player.copy(hand = player.hand.filterNot(_ == card)))
        newDiscardPile <- StateT.pure[F, GameState, List[Card]](card :: gs.discardPile)
        _ <- updateCurrentPlayer[F](newPlayer).modify { gs =>
          discardedLens.set(true)(gs).copy(discardPile = newDiscardPile)
        }
      } yield ()
    }

  // TODO: Move to another file
  def sweepDiscard(hand: Hand, card: Card, discardPile: List[Card]): List[Card] =
    hand ::: discardPile.takeWhile(_ != card)

  // TODO: Holy frick no explanation needed other than prevent printing hand twice when points are played.
  // TODO: Allow player to specify top card with take discard.
  def takeDiscard[F[_] : Sync](untilWithPlay: Option[(Card, List[Card])]): Game[F, Unit] =
    performOnCondition[F](Conditions.takeDiscard(untilWithPlay)) {
      for {
        // TODO: Change call to discardPile.head just in case discard pile ends up being empty. Add error handling. Also ensure player can play card if it's not the head to allow sweeping up the pile.
        discardPile <- StateT.inspect[F, GameState, List[Card]](_.discardPile)
        card <- StateT.pure[F, GameState, Card](untilWithPlay.map(_._1).getOrElse(discardPile.head))
        drawn <- StateT.pure[F, GameState, List[Card]](discardPile.takeWhile(_ != card) ::: List(card))
        newDiscardPile <- StateT.pure[F, GameState, List[Card]](discardPile.filterNot(drawn.contains))
        _ <- modifyCurrentPlayer[F] { player =>
          player.copy(hand = player.hand ::: drawn)
        }
        _ <- StateT.modify[F, GameState](_.copy(discardPile = newDiscardPile))
        _ <- StateT.modify[F, GameState](hasTakenLens.set(true))
        _ <- {
          if (untilWithPlay.isDefined)
            for {
              _ <- points[F](untilWithPlay.map(x => x._1 :: x._2).get, displayChange = false)
              isHandEmpty <- currentPlayer[F].map(_.hand.isEmpty)
              _ <- {
                if (isHandEmpty) StateT.modify[F, GameState](discardedLens.set(true))
                else StateT.pure[F, GameState, Unit]()
              }
            } yield ()
          else
            StateT.pure[F, GameState, Unit]()
        }
        _ <- showCurrentHand[F]
        _ <- showDiscardPile[F]
      } yield ()
    }

  // TODO: Implement grouping own points
  def points[F[_] : Sync](cards: List[Card], displayChange: Boolean = true): Game[F, Unit] =
    performOnCondition[F](Conditions.points[F](cards)) {
      val ordered =
        if (Trick.isRun(cards))
          Card.sort(cards)
        else
          cards

      val doDisplay =
        if (displayChange)
          for {
            _ <- showCurrentHand[F]
            _ <- showCurrentPlayerPoints[F]
          } yield ()
        else
          StateT.pure[F, GameState, Unit]()

      if (Trick.isTrick(cards))
        for {
          player <- currentPlayer[F]
          _ <- updateCurrentPlayer[F](
            player.copy(
              hand = player.hand.filterNot(cards.contains),
              points = player.points ::: List(Trick(ordered, Nil))
            )
          )
          _ <- doDisplay
        } yield ()
      else
        for {
          player <- currentPlayer[F]
          points <- StateT.inspect[F, GameState, List[Trick]](_.players.flatMap(_.points))
          basedOn <- StateT.pure[F, GameState, Trick](points.find(Trick.canPlayOffTrick(cards, _)).get)
          _ <- updateCurrentPlayer[F](
            player.copy(
              hand = player.hand.filterNot(cards.contains),
              points = player.points ::: List(Trick.fromParent(cards, basedOn))
            )
          )
          _ <- doDisplay
        } yield ()
    }

  def snatch[F[_] : Sync]: Game[F, Unit] =
    performOnCondition[F](Conditions.snatch[F]) {
      for {
        discard <- StateT.inspect[F, GameState, List[Card]](_.discardPile)
        card <- StateT.pure[F, GameState, Card](discard.head)
        newDiscard <- StateT.pure[F, GameState, List[Card]](discard.tail)
        points <- StateT.inspect[F, GameState, List[Trick]](_.players.flatMap(_.points))
        basedOn <- StateT.pure[F, GameState, Trick](points.find(Trick.canPlayOffTrick(card :: Nil, _)).get)
        _ <- modifyCurrentPlayer[F] { p =>
          p.copy(points = p.points ::: List(Trick.fromParent(card :: Nil, basedOn)))
        }
        _ <- StateT.modify[F, GameState](_.copy(discardPile = newDiscard))
        _ <- showDiscardPile[F]
        _ <- showCurrentPlayerPoints[F]
      } yield ()
    }

  def help[F[_] : Sync]: Game[F, Unit] =
    writeLine[F]("Valid commands: draw, discard, take discard, points, snatch, help, view, and quit.")

  def view[F[_] : Sync](what: View.What): Game[F, Unit] = {
    import View._

    what match {
      case Hand => showCurrentHand[F]
      case DiscardPile => showDiscardPile[F]
      case Points => showAllPoints[F]
      case All => showAllTurnInformation[F]
    }
  }

  def quit[F[_] : Sync]: Game[F, Unit] =
    readUntilParse[F, Boolean](
      "Are you sure you want to quit? (y/n) ",
      "Invalid input!"
    ) { input =>
      input.trim.toLowerCase match {
        case "y" => Some(true)
        case "n" => Some(false)
        case _ => None
      }
    }.flatMap(q => StateT.modify[F, GameState](quitInvokedLens.set(q)))

  def readUntilParse[F[_] : Sync, A](prompt: String, onFail: => String)(f: String => Option[A]): Game[F, A] = {
    def continuePoint: Game[F, A] =
      readLine[F](prompt).flatMap { input =>
        f(input).map { a =>
          StateT.pure[F, GameState, A](a)
        }.getOrElse(
          writeLine[F](onFail).flatMap(_ => continuePoint)
        )
      }

    continuePoint
  }
}
