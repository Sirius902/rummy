package com.gitlab.sirius902.rummy.game.actionhandlers

import cats._
import cats.data.StateT
import cats.effect._
import com.gitlab.sirius902.rummy.card.{Card, Trick}
import com.gitlab.sirius902.rummy.game.Rummy.Game
import com.gitlab.sirius902.rummy.game.{GameState, Player, Rummy, TurnFlags}
import com.gitlab.sirius902.rummy.io.RummyIO._

/**
  * When a given condition returns a Some[String] it is an error message representing
  * the condition failed while None means the condition passed.
  */
object Conditions {
  def draw[F[_] : Monad]: Game[F, Option[String]] =
    StateT.inspect[F, GameState, Option[String]] { gs =>
      if (gs.turnFlags.hasTaken)
        Some("You can't draw or take discard twice in one turn!")
      else
        None
    }

  def discard[F[_] : Monad](card: Card): Game[F, Option[String]] =
    for {
      flags <- StateT.inspect[F, GameState, TurnFlags](_.turnFlags)
      player <- Rummy.currentPlayer[F]
    } yield {
      if (!player.hand.contains(card))
        Some(s"You do not have the $card!")
      else if (!flags.hasTaken)
        Some("You must first draw or take from the discard pile.")
      else
        None
    }

  // TODO: Cleanup and add taking discard if you can play it off of current points.
  def takeDiscard[F[_] : Monad](untilWithPlay: Option[(Card, List[Card])]): Game[F, Option[String]] =
    for {
      flags <- StateT.inspect[F, GameState, TurnFlags](_.turnFlags)
      discardPile <- StateT.inspect[F, GameState, List[Card]](_.discardPile)
      hand <- Rummy.currentPlayer[F].map(_.hand)
    } yield {
      if (flags.hasTaken)
        Some("You can't draw or take discard twice in one turn!")
      else if (untilWithPlay.isDefined && !untilWithPlay.map(_._1).exists(discardPile.contains))
        Some(s"The discard pile does not contain the card ${untilWithPlay.map(_._1).get}!")
      else if (untilWithPlay.isDefined && discardPile.headOption.exists(h => untilWithPlay.map(_._1).contains(h)))
        Some("Use \"take discard\" to take the top card on the discard pile.")
      else if (untilWithPlay.isDefined && !untilWithPlay.exists { case (c, p) => p.forall(ActionHandlers.sweepDiscard(hand, c, discardPile).contains) })
        Some(s"You won't have at least one of ${Card.formatCards(untilWithPlay.map(_._2).get)} after sweeping the discard pile!")
      else if (untilWithPlay.isDefined && !untilWithPlay.exists { case (c, p) => Trick.isTrick(c :: p) }) {
        val (c, p) = untilWithPlay.get
        Some(s"${Card.formatCards(c :: p)} is not a valid trick!")
      }
      else
        None
    }

  def points[F[_] : Monad](cards: List[Card]): Game[F, Option[String]] =
    for {
      currentPlayer <- Rummy.currentPlayer[F]
      players <- StateT.inspect[F, GameState, List[Player]](_.players)
    } yield {
      if (!cards.forall(currentPlayer.hand.contains))
        Some(s"You do not have at least one of the cards ${Card.formatCards(cards)} in your hand!")
      else if (Trick.canPlayOffPlayers(cards, players))
        None
      else if (!Trick.isTrick(cards))
        Some(s"You can not play ${Card.formatCards(cards)}!")
      else
        None
    }

  def snatch[F[_] : Monad]: Game[F, Option[String]] =
    for {
      players <- StateT.inspect[F, GameState, List[Player]](_.players)
      card <- StateT.inspect[F, GameState, Card](_.discardPile.head)
    } yield {
      if (!Trick.canPlayOffPlayers(card :: Nil, players))
        Some(s"${Card.formatCards(card :: Nil)} can not be played!")
      else
        None
    }

  /**
    * When condition fails, the error will be printed to the user, else will perform the action handler.
    */
  def performOnCondition[F[_] : Sync](condition: Game[F, Option[String]])(handler: => Game[F, Unit]): Game[F, Unit] =
    condition.flatMap(error => error.map(writeLine[F]).getOrElse(handler))
}
