package com.gitlab.sirius902.rummy.game

import monocle.Lens
import monocle.macros.GenLens

case class TurnFlags(
                    // True once the current player has drawn or taken from the discard pile
                    hasTaken: Boolean,
                    // True once the current player has discarded a card successfully
                    discarded: Boolean,
                    // True if the current player invokes a quit command
                    quitInvoked: Boolean,
                    // True if a player runs out of cards or if the deck and draw pile are both empty
                    gameOver: Boolean
                    )

object TurnFlags {
  lazy val initial: TurnFlags =
    TurnFlags(
      hasTaken = false,
      discarded = false,
      quitInvoked = false,
      gameOver = false
    )

  val quitLens: Lens[TurnFlags, Boolean] = GenLens[TurnFlags](_.quitInvoked)
  val discardedLens: Lens[TurnFlags, Boolean] = GenLens[TurnFlags](_.discarded)
  val hasTakenLens: Lens[TurnFlags, Boolean] = GenLens[TurnFlags](_.hasTaken)
  val gameOverLens: Lens[TurnFlags, Boolean] = GenLens[TurnFlags](_.gameOver)
}
