package com.gitlab.sirius902.rummy.game

import com.gitlab.sirius902.rummy.card.Card

object Types {
  type Deck = List[Card]
  type Hand = List[Card]
  type PlayerIndex = Int
}
