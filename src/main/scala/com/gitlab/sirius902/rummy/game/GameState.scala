package com.gitlab.sirius902.rummy.game

import com.gitlab.sirius902.rummy.card.Card
import com.gitlab.sirius902.rummy.game.Types.{Deck, PlayerIndex}
import monocle.Lens
import monocle.macros.GenLens

case class GameState(
                      deck: Deck,
                      discardPile: List[Card],
                      players: List[Player],
                      currentPlayerIndex: PlayerIndex,
                      turnFlags: TurnFlags
                    )

object GameState {
  def initializePlayers(n: Int): List[Player] =
    List.fill(n)(Player.initial)

  def create(players: Int): GameState =
    GameState(
      deck = Nil,
      discardPile = Nil,
      players = initializePlayers(players),
      currentPlayerIndex = 0,
      turnFlags = TurnFlags.initial
    )

  def playerDisplayIdFromIndex(i: PlayerIndex): String =
    (i + 1).toString

  def getCurrentPlayer(gs: GameState): Player = gs.players(gs.currentPlayerIndex)

  def updateCurrentPlayer(updatedPlayer: Player)(gs: GameState): GameState =
    gs.copy(
      players = gs.players.updated(
        gs.currentPlayerIndex,
        updatedPlayer
      )
    )

  def switchToNextPlayer(gs: GameState): GameState =
    gs.copy(currentPlayerIndex = (gs.currentPlayerIndex + 1) % gs.players.length)

  val turnFlagsLens: Lens[GameState, TurnFlags] = GenLens[GameState](_.turnFlags)
  val quitInvokedLens: Lens[GameState, Boolean] = turnFlagsLens composeLens TurnFlags.quitLens
  val discardedLens: Lens[GameState, Boolean] = turnFlagsLens composeLens TurnFlags.discardedLens
  val hasTakenLens: Lens[GameState, Boolean] = turnFlagsLens composeLens TurnFlags.hasTakenLens
  val gameOverLens: Lens[GameState, Boolean] = turnFlagsLens composeLens TurnFlags.gameOverLens
}
