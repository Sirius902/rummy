package com.gitlab.sirius902.rummy.algorithm

object Algorithms {
  // TODO: Make sameAll safe for one argument if it isn't already
  def sameAll[A](as: List[A])(f: (A, A) => Boolean): Boolean = as match {
    case h :: t => t.forall(a => f(a, h))
    case _ => true
  }
}
