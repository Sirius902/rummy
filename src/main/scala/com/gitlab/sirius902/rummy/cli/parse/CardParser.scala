package com.gitlab.sirius902.rummy.cli.parse

import cats.implicits._
import com.gitlab.sirius902.rummy.card.{Card, Rank, Suit}

object CardParser {

  def parseRank(input: String): Option[Rank] =
    Rank.all.find(_.inputSymbol.equalsIgnoreCase(input))

  def parseSuit(input: String): Option[Suit] =
    Suit.all.find(_.inputSymbol.equalsIgnoreCase(input))

  def parseCard(input: String): Option[Card] = {
    if (input.length > 3)
      None

    val rank = for {
      c <- Some(input.dropRight(1))
      str <- Some(c.toString)
      r <- parseRank(str)
    } yield r

    val suit = for {
      c <- input.reverse.headOption
      str <- Some(c.toString)
      s <- parseSuit(str)
    } yield s

    for {
      r <- rank
      s <- suit
    } yield Card(r, s)
  }

  def parseCards(input: String): Option[List[Card]] =
    input.split(" ").map(parseCard).toList.sequence
}
