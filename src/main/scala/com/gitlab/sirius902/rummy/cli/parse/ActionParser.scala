package com.gitlab.sirius902.rummy.cli.parse

import com.gitlab.sirius902.rummy.cli.parse.CardParser._
import com.gitlab.sirius902.rummy.playeraction.Action
import com.gitlab.sirius902.rummy.playeraction.Action.View.What
import com.gitlab.sirius902.rummy.playeraction.Action._

import scala.util.matching.Regex

object ActionParser {

  private val discardRegex: Regex = """discard (.+)""".r
  private val takeDiscardRegex: Regex = """take discard (.+)""".r
  private val pointsRegex: Regex = """points (.+)""".r
  private val viewRegex: Regex = """view (.+)""".r

  def parse(input: String): Option[Action] = input.trim.toLowerCase match {
    case "draw" => Some(Draw)
    case "snatch" => Some(Snatch)
    case discardRegex(card) => parseCard(card).map(Discard.apply)
    case "take discard" => Some(TakeDiscard(None))
    case takeDiscardRegex(cards) =>
      for {
        cards <- parseCards(cards)
        head <- cards.headOption
        tail <- Some(cards.tail)
      } yield TakeDiscard(Some(head, tail))
    case pointsRegex(cards) => parseCards(cards).map(Points.apply)
    case "help" => Some(Help)
    case viewRegex(what) => parseWhat(what).map(View.apply)
    case "quit" => Some(QuitGame)
    case _ => None
  }

  private def parseWhat(input: String): Option[What] =
    input.trim.toLowerCase match {
      case "hand" => Some(View.Hand)
      case "discard" => Some(View.DiscardPile)
      case "points" => Some(View.Points)
      case "all" => Some(View.All)
      case _ => None
    }
}
