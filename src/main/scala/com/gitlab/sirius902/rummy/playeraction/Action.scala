package com.gitlab.sirius902.rummy.playeraction

import com.gitlab.sirius902.rummy.card.Card

trait Action

object Action {

  case object Draw extends Action

  case class Discard(card: Card) extends Action

  // Left of tuple is card player wants to draw from discard pile, right is what player will use to play that card
  case class TakeDiscard(untilWithPlay: Option[(Card, List[Card])]) extends Action

  case class Points(cards: List[Card]) extends Action

  case object Snatch extends Action

  case object Help extends Action

  case class View(what: View.What) extends Action

  object View {

    trait What

    case object Hand extends What

    case object DiscardPile extends What

    case object Points extends What

    case object All extends What

  }

  case object QuitGame extends Action

}
